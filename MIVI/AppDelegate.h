//
//  AppDelegate.h
//  MIVI
//
//  Created by Soundrapandian on 21/02/18.
//  Copyright © 2018 Soundrapandian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplashControllerViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) SplashControllerViewController *splashViewController;
@end

