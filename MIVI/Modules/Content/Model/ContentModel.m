//
//  ContentModel.m
//  MIVI
//
//  Created by Soundrapandian on 21/02/18.
//  Copyright © 2018 Soundrapandian. All rights reserved.
//

#import "ContentModel.h"

@implementation ContentModel

-(id)init
{
    self=[super init];
    if(self)
        self.dataDictionary=[self fetchDictionaryFromFile];
    return self;
}

-(NSDictionary *)fetchDictionaryFromFile
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"collection" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}
-(NSMutableArray *)fetchSubsriptionDetails
{
//Tried For fetch subscripsions dynamically if we change json file in future
//    NSArray *includedArray= [self.dataDictionary objectForKey:@"included"];
//    NSMutableArray *subscriptionArray =[[NSMutableArray alloc]init];
//    
// 
//    for(NSDictionary *obj in includedArray)
//    {
//        if([[obj objectForKey:@"type"] isEqualToString:@"subscriptions"])
//            [subscriptionArray addObject:[obj objectForKey:@"attributes"]];
//    }
//    return subscriptionArray;
    NSMutableArray *subscriptionContentArray=[[NSMutableArray alloc]init];
    
    self.subscriptionDictionary=[[self.dataDictionary objectForKey:@"included"]objectAtIndex:1];
    
    [subscriptionContentArray addObject:[NSString stringWithFormat:@"%@",[[self.subscriptionDictionary objectForKey:@"attributes"] objectForKey:@"included-data-balance"]]];
    [subscriptionContentArray addObject:[[self.subscriptionDictionary objectForKey:@"attributes"] objectForKey:@"expiry-date"]];
    return subscriptionContentArray;
}

-(NSMutableArray *)fetchProductsDetails
{
    NSString *subscribtionRelatedProductID=[[[[self.subscriptionDictionary objectForKey:@"relationships"]objectForKey:@"product"]objectForKey:@"data"]objectForKey:@"id"];
    
     NSMutableArray *productArray =[[NSMutableArray alloc]init];

    NSArray *includedArray= [self.dataDictionary objectForKey:@"included"];
    for(NSDictionary *obj in includedArray)
    {
        if([[obj objectForKey:@"type"] isEqualToString:@"products"] && [[obj objectForKey:@"id"] isEqualToString:subscribtionRelatedProductID])
        {
            [productArray addObject:[[obj objectForKey:@"attributes"]objectForKey:@"name"]];
            [productArray addObject:[[obj objectForKey:@"attributes"]objectForKey:@"price"]];
        }
    }
    return productArray;
}

-(NSMutableArray *)fetchUserInfoDetails
{
    NSDictionary *infoDictionary=[[self.dataDictionary objectForKey:@"data"]objectForKey:@"attributes"];
    
    NSMutableArray *infoArray =[[NSMutableArray alloc]init];
    [infoArray addObject:[infoDictionary objectForKey:@"first-name"]];
    [infoArray addObject:[infoDictionary objectForKey:@"last-name"]];
    [infoArray addObject:[infoDictionary objectForKey:@"date-of-birth"]];
    [infoArray addObject:[infoDictionary objectForKey:@"contact-number"]];
    
    return infoArray;
}
@end
