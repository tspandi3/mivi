//
//  ContentModel.h
//  MIVI
//
//  Created by Soundrapandian on 21/02/18.
//  Copyright © 2018 Soundrapandian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContentModel : NSObject

@property(strong,nonatomic)NSDictionary *dataDictionary;

@property(strong,nonatomic)NSDictionary *subscriptionDictionary;


-(NSMutableArray *)fetchSubsriptionDetails;
-(NSMutableArray *)fetchProductsDetails;
-(NSMutableArray *)fetchUserInfoDetails;
@end
