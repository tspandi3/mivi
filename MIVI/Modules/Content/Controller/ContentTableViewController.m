//
//  ContentTableViewController.m
//  MIVI
//
//  Created by Soundrapandian on 21/02/18.
//  Copyright © 2018 Soundrapandian. All rights reserved.
//

#import "ContentTableViewController.h"

@interface ContentTableViewController ()

@end

@implementation ContentTableViewController

-(id)init
{
    self=[super init];
    if(self)
    {
        self.contentModel=[[ContentModel alloc]init];
        self.subscriptionTitleArray=[[NSMutableArray alloc]initWithObjects:@"Balance", @"Exp.Date",nil];
        self.productsTitleArray=[[NSMutableArray alloc]initWithObjects:@"Name", @"Price",nil];
        self.userInfoTitleArray=[[NSMutableArray alloc]initWithObjects:@"First Name", @"Last Name",@"DOB",@"Contact",nil];
        [self createViews];
        
    }
    return self;
}
-(void)createViews
{
    self.contentTableview=[[UITableView alloc]init];
    self.contentTableview.frame=CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y+50,self.view.frame.size.width,self.view.frame.size.height-120);
    [self.view addSubview:self.contentTableview];
    
    self.btnLogout=[UIButton buttonWithType:UIButtonTypeCustom];
    self.btnLogout.frame=CGRectMake(self.view.center.x-70/2,self.contentTableview.frame.origin.y+self.contentTableview.frame.size.height+10,70,40);
    self.btnLogout.backgroundColor=[UIColor colorWithRed:32/255.0f green:132/255.0f blue:185/255.0f alpha:1.0f];
    [self.btnLogout setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnLogout setTitle:@"Logout" forState:UIControlStateNormal];
    [self.btnLogout addTarget:self action:@selector(clickLogout) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.btnLogout];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.subscriptionArray=[self.contentModel fetchSubsriptionDetails];
    self.productsArray=[self.contentModel fetchProductsDetails];
    self.userInfoArray=[self.contentModel fetchUserInfoDetails];
        self.contentTableview.delegate=self;
    self.contentTableview.dataSource=self;
    [self.contentTableview reloadData];

}
-(void)clickLogout
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0)
        return self.subscriptionArray.count;
    if (section == 1)
        return self.productsArray.count;
    if (section == 2)
        return self.userInfoArray.count;
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    
    if (indexPath.section == 0)
    {
        cell.textLabel.text = [self.subscriptionTitleArray objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [self.subscriptionArray objectAtIndex:indexPath.row];
    }
    
    if (indexPath.section == 1)
    {
        cell.textLabel.text = [self.productsTitleArray objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [self.productsArray objectAtIndex:indexPath.row];
    }
    if(indexPath.section == 2)
    {
        cell.textLabel.text = [self.userInfoTitleArray objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [self.userInfoArray objectAtIndex:indexPath.row];
    }
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
        if (section == 0)
            return @"Subscriptions";
        if (section == 1)
            return @"Products";
        if (section == 2)
            return @"User Info";
        return @"undefined";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
