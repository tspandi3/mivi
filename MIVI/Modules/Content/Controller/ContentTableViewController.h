//
//  ContentTableViewController.h
//  MIVI
//
//  Created by Soundrapandian on 21/02/18.
//  Copyright © 2018 Soundrapandian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContentModel.h"

@interface ContentTableViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) ContentModel *contentModel;

@property (strong, nonatomic) UITableView *contentTableview;
@property (strong, nonatomic) UIButton *btnLogout;

@property(strong,nonatomic)NSMutableArray *subscriptionTitleArray;
@property(strong,nonatomic)NSMutableArray *productsTitleArray;
@property(strong,nonatomic)NSMutableArray *userInfoTitleArray;

@property(strong,nonatomic)NSMutableArray *subscriptionArray;
@property(strong,nonatomic)NSMutableArray *productsArray;
@property(strong,nonatomic)NSMutableArray *userInfoArray;
@end
