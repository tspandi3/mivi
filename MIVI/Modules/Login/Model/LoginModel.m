//
//  LoginModel.m
//  MIVI
//
//  Created by Soundrapandian on 21/02/18.
//  Copyright © 2018 Soundrapandian. All rights reserved.
//

#import "LoginModel.h"

@implementation LoginModel

-(id)init
{
    self=[super init];
    if(self)
        self.dataDictionary=[self fetchDictionaryFromFile];
    return self;
}

-(NSDictionary *)fetchDictionaryFromFile
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"collection" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}
-(NSString *)fetchUserID
{
   return [[[[self.dataDictionary objectForKey:@"included"] objectAtIndex:0] objectForKey:@"attributes"]objectForKey:@"msn"];
}
@end
