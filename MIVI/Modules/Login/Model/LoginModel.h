//
//  LoginModel.h
//  MIVI
//
//  Created by Soundrapandian on 21/02/18.
//  Copyright © 2018 Soundrapandian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginModel : NSObject

@property(strong,nonatomic)NSDictionary *dataDictionary;

-(NSString *)fetchUserID;
@end
