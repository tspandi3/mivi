//
//  ViewController.h
//  MIVI
//
//  Created by Soundrapandian on 21/02/18.
//  Copyright © 2018 Soundrapandian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginView.h"
#import "LoginModel.h"
#import "ContentTableViewController.h"


@interface LoginViewController : UIViewController<LoginViewClassDelegate>

@property (strong, nonatomic) ContentTableViewController *contentViewController;

@property (strong, nonatomic) LoginModel *loginModel;
@property (strong, nonatomic) LoginView *loginView;
@end

