//
//  ViewController.m
//  MIVI
//
//  Created by Soundrapandian on 21/02/18.
//  Copyright © 2018 Soundrapandian. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

-(id)init
{
    self=[super init];
    if(self)
        self.loginModel=[[LoginModel alloc]init];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.loginView=[[LoginView alloc]initWithFrame:self.view.bounds];
    self.loginView.delegate=self;
    self.view=self.loginView;
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)checkLogin:(NSString *)userID
{
    if([[self.loginModel fetchUserID] isEqualToString:userID])
    {
        self.loginView.txtID.text=@"";
        self.contentViewController=[[ContentTableViewController alloc]init];
        self.contentViewController.modalTransitionStyle=UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:self.contentViewController animated:YES completion:nil];
    }
    else
    {
        self.loginView.txtID.text=@"";
        [self displayErrorMssg:@"The MSN ID is incorrect"];
    }
}
-(void)displayErrorMssg:(NSString *)mssg
{
    UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"MIVI" message:mssg preferredStyle:UIAlertControllerStyleAlert];
                                     
    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault                                handler:^(UIAlertAction * action) { }];
    [errorAlert addAction:cancelButton];
                                     
    [self presentViewController:errorAlert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
