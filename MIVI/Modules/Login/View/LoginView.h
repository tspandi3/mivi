//
//  LoginView.h
//  MIVI
//
//  Created by Soundrapandian on 21/02/18.
//  Copyright © 2018 Soundrapandian. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoginViewClassDelegate
@required
-(void)checkLogin:(NSString *)userID;
-(void)displayErrorMssg:(NSString *)mssg;
@end


@interface LoginView : UIView<UITextFieldDelegate>

@property (strong, nonatomic) UILabel *lblTitle;
@property (strong, nonatomic) UILabel *lblID;
@property (strong, nonatomic) UITextField *txtID;
@property (strong, nonatomic) UIButton *btnLogin;

@property (nonatomic, assign) id<LoginViewClassDelegate> delegate;

@end
