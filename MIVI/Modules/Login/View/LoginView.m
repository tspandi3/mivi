//
//  LoginView.m
//  MIVI
//
//  Created by Soundrapandian on 21/02/18.
//  Copyright © 2018 Soundrapandian. All rights reserved.
//

#import "LoginView.h"
#import "AppMacros.h"

@implementation LoginView

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self) {
        self.backgroundColor=[UIColor whiteColor];
        [self createViews];
        
    }
    return self;
}
-(void)createViews
{
    CGFloat offSetY = 100;
    
    self.lblTitle=[[UILabel alloc]initWithFrame:CGRectMake((self.frame.size.width-ProductLabelWidth)/2,offSetY,ProductLabelWidth,ProductLabelHeight)];
    self.lblTitle.backgroundColor=[UIColor clearColor];
    self.lblTitle.text=@"MIVI";
    self.lblTitle.textColor=[UIColor colorWithRed:32/255.0f green:132/255.0f blue:185/255.0f alpha:1.0f];
    self.lblTitle.font=[UIFont boldSystemFontOfSize:40.0f];
    [self addSubview:self.lblTitle];
    
    offSetY+=self.lblTitle.frame.size.height+20;
    
    self.lblID=[[UILabel alloc]initWithFrame:CGRectMake(self.frame.origin.x+10,offSetY,80,40)];
    self.lblID.backgroundColor=[UIColor clearColor];
    self.lblID.text=@"MSN ID :";
    self.lblID.textColor=[UIColor blackColor];
    self.lblID.font=[UIFont systemFontOfSize:15.0f];
    [self addSubview:self.lblID];
    
    UIView *leftpaddingView = [[UIView alloc] initWithFrame:CGRectMake(0,0,5,40)];
    
    self.txtID=[[UITextField alloc]initWithFrame:CGRectMake(self.lblID.frame.origin.x+self.lblID.frame.size.width+10,offSetY,self.frame.size.width-40-80,40)];
    self.txtID.backgroundColor=[UIColor clearColor];
    self.txtID.textColor=[UIColor blackColor];
    self.txtID.font=[UIFont systemFontOfSize:15.0f];
    self.txtID.keyboardType=UIKeyboardTypeNumberPad;
    self.txtID.leftView=leftpaddingView;
    self.txtID.leftViewMode=UITextFieldViewModeAlways;
    self.txtID.layer.borderColor=[UIColor colorWithRed:32/255.0f green:132/255.0f blue:185/255.0f alpha:1.0f].CGColor;
    self.txtID.layer.borderWidth=1.0f;
    self.txtID.placeholder=@"Enter your MSN ID";
    self.txtID.returnKeyType=UIReturnKeyDone;
    [self addSubview:self.txtID];
    
    offSetY+=self.lblID.frame.size.height+20;
    
    self.btnLogin=[UIButton buttonWithType:UIButtonTypeCustom];
    self.btnLogin.frame=CGRectMake(self.center.x-70/2,offSetY,70,40);
    self.btnLogin.backgroundColor=[UIColor colorWithRed:32/255.0f green:132/255.0f blue:185/255.0f alpha:1.0f];
    [self.btnLogin setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnLogin setTitle:@"Login" forState:UIControlStateNormal];
    [self.btnLogin addTarget:self action:@selector(clickLogin) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btnLogin];

}
-(void)clickLogin
{
    if([self.txtID.text length]>0)
    {
        if (self.delegate!=nil)
            [self.delegate checkLogin:self.txtID.text];
    }
    else
    {
        if (self.delegate!=nil)
            [self.delegate displayErrorMssg:@"The MSN ID is empty."];
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
