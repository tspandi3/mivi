//
//  SplashControllerViewController.h
//  MIVI
//
//  Created by Soundrapandian on 21/02/18.
//  Copyright © 2018 Soundrapandian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"

@interface SplashControllerViewController : UIViewController


@property (strong, nonatomic) UILabel *lblProductName;

@property (strong, nonatomic) LoginViewController *loginViewController;
@end
