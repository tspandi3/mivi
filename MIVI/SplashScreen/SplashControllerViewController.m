//
//  SplashControllerViewController.m
//  MIVI
//
//  Created by Soundrapandian on 21/02/18.
//  Copyright © 2018 Soundrapandian. All rights reserved.
//

#import "SplashControllerViewController.h"
#import "AppMacros.h"

@interface SplashControllerViewController ()

@end

@implementation SplashControllerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view=[[UIView alloc]initWithFrame:self.view.bounds];
    self.view.backgroundColor=[UIColor whiteColor];
    
    self.lblProductName=[[UILabel alloc]initWithFrame:CGRectMake(self.view.center.x-ProductLabelWidth/2,self.view.center.y-ProductLabelHeight/2,ProductLabelWidth,ProductLabelHeight)];
    self.lblProductName.backgroundColor=[UIColor clearColor];
    self.lblProductName.text=@"MIVI";
    self.lblProductName.textColor=[UIColor colorWithRed:32/255.0f green:132/255.0f blue:185/255.0f alpha:1.0f];
    self.lblProductName.font=[UIFont boldSystemFontOfSize:40.f];
    [self.view addSubview:self.lblProductName];
    
    [self performSelector:@selector(hideSplash) withObject:nil afterDelay:3.0]; // Now dismiss with time delay. We can do the initialization in future
    
}

-(void)hideSplash
{
    self.loginViewController = [[LoginViewController alloc]init];
    self.loginViewController.modalTransitionStyle=UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:self.loginViewController animated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
